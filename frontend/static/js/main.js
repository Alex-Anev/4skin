// https://youtu.be/6BozpmSjk-Y?t=1386

// SECTION router

const router = async () => {
  const routes = [
    {
      path: '/home',
      view: () => {
        clearPage
        let t = printHtml('home')
        console.log(t)
      },
    },
    {
      path: '/about',
      view: () => {
        printHtml('about')
      },
    },
    {
      path: '/settings',
      view: () => {
        printHtml('settings')
      },
    },
    // { path: '/other', view: () => console.log('other') },
  ]

  // NOTE check which is current url

  const potentialMatches = routes.map((route) => {
    return {
      route: route,
      // NOTE returns true when current path is the same as path in url
      isMatch: location.pathname === route.path,
    }
  })

  let match = potentialMatches.find((potentialMatch) => potentialMatch.isMatch)
  !match && (match = { route: routes[0], isMatch: true })
  console.log('current page: ', match.route.path)

  // FIXME
  // const view = new match.route.view(getParams(match))
  // document.getElementById('body').innerHTML = await view.getHtml()
}

// NOTE stores last page so you can return in browser

const navigateTo = (url) => {
  history.pushState(null, null, url)
  router()
}

document.addEventListener('DOMContentLoaded', () => {
  document.body.addEventListener('click', (e) => {
    console.log('body click')
    if (e.target.matches('[data-link]')) {
      e.preventDefault()
      navigateTo(e.target.href)
    }
  })
  router()
})

window.addEventListener('popstate', router)

// !SECTION
// SECTION document

const doc = document.body
doc.className = 'w3-2017-shaded-spruce'
doc.style = 'height:100%;'

// NOTE generates <a> link at the top for every string in the array

const links = ['/home', '/about', '/settings', '/other']

links.forEach((e) => {
  let a = document.createElement('a')
  a.setAttribute('data-link', '') // need to set this for non-reload routing
  a.href = e
  a.innerText = `${e}`
  a.className = 'w3-container w3-btn'
  doc.appendChild(a)
})

// header
// TODO add css effects & animations on hover

const header = document.createElement('header')
header.className = 'w3-container w3-light-gray'
doc.appendChild(header)

// title

const h1 = document.createElement('h1')
h1.setAttribute('class', 'w3-monospace')
h1.append('4s')

header.append(h1)

header.addEventListener('click', (e) => {
  console.log('header click')
})

// !SECTION
// SECTION pages
// this frame is used to load pages in and out of for display

const pageFrame = document.createElement('pageFrame')
pageFrame.className = 'w3-container'
pageFrame.innerText = 'i am pageFrame'
doc.appendChild(pageFrame)

// NOTE all different pages

const printHome = () => {
  let home = document.createElement('home')
  home.id = 'home'
  home.className = 'w3-container'
  home.innerText = 'home'
  pageFrame.appendChild(home)
}

const printAbout = () => {
  let about = document.createElement('about')
  about.id = 'about'
  about.className = 'w3-container'
  about.innerText = 'about'
  pageFrame.appendChild(about)
}

const printSettings = () => {
  let settings = document.createElement('settings')
  settings.id = 'settings'
  settings.className = 'w3-container'
  settings.innerText = 'settings'
  pageFrame.appendChild(settings)
}

const printHtml = (p) => {
  clearPage()
  switch (p) {
    case 'home':
      printHome()
      break
    case 'about':
      printAbout()
      break
    case 'settings':
      printSettings()
      break
    default:
      console.log('invalid printHtml() param')
  }
}

const clearPage = () => {
  while (document.querySelectorAll('pageFrame').lastChild) {
    removeChild(document.querySelectorAll('pageFrame').lastChild)
  }
}

// ! WIP
// NOTE dom object generator
/** TODO add more options
 / - choose what DOM element to attach new elem to
 / - more options
 / - choose between different CSS library's
*/
const makeElement = (type, text, css) => {
  let dom_obj = document.createElement(`${type}`)
  dom_obj.className = `${css}`
  typeof text === 'string' || text instanceof String
    ? dom_obj.append(text)
    : console.log('text should be string')
  return dom_obj
}

// doc.append(makeElement('div', 'i am element', 'w3-container w3-red'))

// !SECTION

console.log('end of file')

// NOTE
// https://www.javascripttutorial.net/javascript-dom/javascript-createelement/
